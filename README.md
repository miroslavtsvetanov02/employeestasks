# EmployeeTasks

EmployeeTasks is a solution for managing tasks and employees

# Authors

- [Miroslav Tsvetanov](https://gitlab.com/miroslavtsvetanov02)

# Technologies

- ASP .NET Core
- ASP .NET Core MVC
- MS SQL Server
- Entity Framework Core
- JavaScript/jQuery
- Bootstrap/Font Awesome
- Microsoft Identity (for the web pages)

# How to setup:

1. First, get the clone link from the clone button.

![First Step](Images/clone.png)

2. Then make a folder for the project.
3. Open Visual Studio and choose clone repository.

![Second Step](Images/repository.png)


4. Use the clone link and select your folder.

![Third Step](Images/clonePath.png)

5. Change the DefaultConnection server to your own

![Fourth Step](Images/appSettings.png)

6.Create a database at your local machine.

![Last Step](Images/createDatabase.png)


And you are finished!
For more info about the project check the documentation in ProjectDocumentation