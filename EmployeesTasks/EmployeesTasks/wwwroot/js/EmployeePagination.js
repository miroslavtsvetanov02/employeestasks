﻿$(document.body).on('click', '.page-numbers', function () {
	var newPageNumber = this.innerHTML;
	$.get('/Employees', { employeePage: newPageNumber }, function (response) {
		$('.employee-list').html($(response).find('.employee-list').html());
	})
})