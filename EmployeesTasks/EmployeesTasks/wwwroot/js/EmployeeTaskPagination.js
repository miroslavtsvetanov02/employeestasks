﻿$(document.body).on('click', '.page-numbers', function () {
	var newPageNumber = this.innerHTML;
	$.get('/EmployeeTasks', { taskPage: newPageNumber }, function (response) {
		$('.task-list').html($(response).find('.task-list').html());
	})
})