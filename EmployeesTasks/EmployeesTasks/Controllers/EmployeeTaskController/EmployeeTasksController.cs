﻿using EmployeesTasks.Service.Services.Contracts;
using EmployeesTasks.ViewModels.EmployeeTasks;
using EmployeesTasks.ViewModels.Mappers;
using Microsoft.AspNetCore.Mvc;
using PagedList;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTasks.Controllers.EmployeeTaskController
{
	public class EmployeeTasksController : Controller
	{
		private readonly IEmployeeTaskService _employeeTaskService;

		public EmployeeTasksController(IEmployeeTaskService employeeTaskService)
		{
			_employeeTaskService = employeeTaskService;
		}

		public async Task<IActionResult> Index(int taskPage = 1)
		{
			var tasks = await _employeeTaskService.GetAllAsync();

			var tasksToShow = tasks.Select(m => m.ToViewModel()).ToPagedList(taskPage, 10);

			var resolute = new EmployeeTasksViewModel()
			{
				Tasks = tasksToShow
			};

			return View(resolute);
		}
	}
}
