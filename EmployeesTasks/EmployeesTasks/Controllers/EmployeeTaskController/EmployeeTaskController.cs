﻿using EmployeesTasks.Service.Exceptions;
using EmployeesTasks.Service.Services.Contracts;
using EmployeesTasks.ViewModels.EmployeeTasks;
using EmployeesTasks.ViewModels.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTasks.Controllers.EmployeeTaskController
{
	public class EmployeeTaskController : Controller
	{
		private readonly IEmployeeTaskService _employeeTaskService;
		private readonly IEmployeeService _employeeService;

		public EmployeeTaskController(IEmployeeTaskService employeeTaskService, IEmployeeService employeeService)
		{
			_employeeTaskService = employeeTaskService;
			_employeeService = employeeService;

        }

		public async Task<IActionResult> Create()
		{
			return View("Create");
		}

		[HttpPost]
		public async Task<IActionResult> Create(EmployeeTaskViewModel taskModel)
		{
			if (!ModelState.IsValid)
			{
				return View("Create", taskModel);
			}

			var task = taskModel.ToDTO();
			await _employeeTaskService.CreateTaskAsync(task);

			return RedirectToAction("Index", "EmployeeTasks");
		}

		public async Task<IActionResult> Edit(int id)
		{
			try
			{
                var task = await _employeeTaskService.GetByIdAsync(id);
                return View(task.ToViewModel());
            }
            catch (EntityNotFoundException)
            {
                return View("Error");
            }
		}

		[HttpPost]
		public async Task<IActionResult> Edit(EmployeeTaskViewModel taskModel)
		{
			var task = taskModel.ToDTO();
			await _employeeTaskService.UpdateTaskAsync(task);

			return RedirectToAction("Index", "EmployeeTasks");
		}

		public async Task<IActionResult> Details(int id)
		{
			try
			{
                var task = await _employeeTaskService.GetByIdAsync(id);
                return View(task.ToViewModel());
            }
			catch (EntityNotFoundException)
			{

                return View("Error");
            }
		}


		[HttpPost]
		public async Task<IActionResult> Delete(int id)
		{
			await _employeeTaskService.DeleteAsync(id);

			return RedirectToAction("Index", "EmployeeTasks");
		}

		public async Task<IActionResult> AssignTask(int id)
		{
            var employees = await _employeeService.GetAllAsync();
            var task = await _employeeTaskService.GetByIdAsync(id);

            var employeesToShow = employees.Select(m => m.ToViewModel()).ToList();

            return View(employeesToShow);
        }

		[HttpPost]
		public async Task<IActionResult> AssignTask(int id, int employeeId)
		{
            var employee = await _employeeService.GetByIdAsync(employeeId);

            await _employeeTaskService.AssignTask(id, employee.Id);

            return RedirectToAction("Index", "EmployeeTasks");
        }

		[HttpPost]
		public async Task<IActionResult> CompleteTask(int id)
		{
			await _employeeTaskService.CompleteTask(id);

			return RedirectToAction("Index", "EmployeeTasks");
		}
	}
}
