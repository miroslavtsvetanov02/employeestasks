﻿using EmployeesTasks.Service.Exceptions;
using EmployeesTasks.Service.Services.Contracts;
using EmployeesTasks.ViewModels.Employees;
using EmployeesTasks.ViewModels.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EmployeesTasks.Controllers.EmployeeController
{
	public class EmployeeController : Controller
	{
		private readonly IEmployeeService _employeeService;

		public EmployeeController(IEmployeeService employeeService)
		{
			_employeeService = employeeService;
		}

		public async Task<IActionResult> Create()
		{
			return View("Create");
		}

		[HttpPost]
		public async Task<IActionResult> Create(EmployeeViewModel employeeModel)
		{
			if (!ModelState.IsValid)
			{
				return View("Create", employeeModel);
			}

			var employee = employeeModel.ToDTO();
			await _employeeService.CreateEmployeeAsync(employee);

			return RedirectToAction("Index", "Employees");
		}

		public async Task<IActionResult> Edit(int id)
		{
			try
			{
                var employee = await _employeeService.GetByIdAsync(id);
                return View(employee.ToViewModel());
            }
			catch (EntityNotFoundException)
			{
				return View("Error");
			}
		}

		[HttpPost]
		public async Task<IActionResult> Edit(EmployeeViewModel employeeModel)
		{
            if (!ModelState.IsValid)
            {
                return View("Edit", employeeModel);
            }

            var employee = employeeModel.ToDTO();
			await _employeeService.UpdateEmployeeAsync(employee);

			return RedirectToAction("Index", "Employees");
		}

		public async Task<IActionResult> Details(int id)
		{
			try
			{
                var employee = await _employeeService.GetByIdAsync(id);
                return View(employee.ToViewModel());
            }
            catch (EntityNotFoundException)
            {

                return View("Error");
            }
        }


		[HttpPost]
		public async Task<IActionResult> Delete(int id)
		{
			await _employeeService.DeleteAsync(id);

			return RedirectToAction("Index", "Employees");
		}
	}
}
