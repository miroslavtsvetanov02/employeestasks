﻿using EmployeesTasks.Service.Services.Contracts;
using EmployeesTasks.ViewModels.Employees;
using EmployeesTasks.ViewModels.Mappers;
using Microsoft.AspNetCore.Mvc;
using PagedList;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTasks.Controllers.EmployeeController
{
	public class EmployeesController : Controller
	{
		private readonly IEmployeeService _employeeService;

		public EmployeesController(IEmployeeService employeeService)
		{
			_employeeService = employeeService;
		}

		public async Task<IActionResult> Index(int employeePage = 1)
		{
			var employees = await _employeeService.GetAllAsync();

			var employeesToShow = employees.Select(m => m.ToViewModel()).ToPagedList(employeePage, 10);

			var resolute = new EmployeesViewModel()
			{
				Employees = employeesToShow
			};

			return View(resolute);
		}

        public async Task<IActionResult> TopEmployees()
        {
            var topEmployees = await _employeeService.GetTopEmployeesAsync();

			var topEmployeesToShow = topEmployees.Select(m => m.ToViewModel()).ToList();

            return View(topEmployeesToShow);
        }
    }
}
