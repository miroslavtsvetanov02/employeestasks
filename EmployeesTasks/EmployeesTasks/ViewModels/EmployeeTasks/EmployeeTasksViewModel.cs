﻿using System.Collections.Generic;

namespace EmployeesTasks.ViewModels.EmployeeTasks
{
	public class EmployeeTasksViewModel
	{
		public IEnumerable<EmployeeTaskViewModel> Tasks { get; set; } = new List<EmployeeTaskViewModel>();
	}
}
