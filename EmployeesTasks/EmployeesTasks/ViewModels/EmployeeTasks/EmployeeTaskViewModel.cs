﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeesTasks.ViewModels.EmployeeTasks
{
    public class EmployeeTaskViewModel
	{
		public int Id { get; set; }

        public int? EmployeeId { get; set; }

        [Required(ErrorMessage = "The Title field is required.")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "The Title length must be between 3 and 30.")]
        public string Title { get; set; }


        [Required(ErrorMessage = "The Name field is required.")]
        [StringLength(200, MinimumLength = 15, ErrorMessage = "The Description length must be between 15 and 200.")]
        public string Description { get; set; }


        [Required(ErrorMessage = "The Deadline field is required.")]
        public DateTime DueDate { get; set; }

		public bool IsDone { get; set; }
	}
}
