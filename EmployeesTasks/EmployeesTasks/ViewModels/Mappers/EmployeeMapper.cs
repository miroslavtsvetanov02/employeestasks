﻿using EmployeesTasks.Service.DTOs;
using EmployeesTasks.ViewModels.Employees;

namespace EmployeesTasks.ViewModels.Mappers
{
	public static class EmployeeMapper
	{
		public static EmployeeViewModel ToViewModel(this EmployeeDTO employeeDTO)
		{
			return new EmployeeViewModel()
			{
				Id = employeeDTO.Id,
				FullName = employeeDTO.FullName,
				Email = employeeDTO.Email,
				PhoneNumber = employeeDTO.PhoneNumber,
				DateOfBirth = employeeDTO.DateOfBirth,
				MonthlySalary = employeeDTO.MonthlySalary,
				CompletedTasks = employeeDTO.CompletedTasks
			};
		}

		public static EmployeeDTO ToDTO(this EmployeeViewModel employeeViewModel)
		{
			return new EmployeeDTO()
			{
				Id = employeeViewModel.Id,
				FullName = employeeViewModel.FullName,
				Email = employeeViewModel.Email,
				PhoneNumber = employeeViewModel.PhoneNumber,
				DateOfBirth = employeeViewModel.DateOfBirth,
				MonthlySalary = employeeViewModel.MonthlySalary,
				CompletedTasks = employeeViewModel.CompletedTasks,
			};
		}
	}
}
