﻿using EmployeesTasks.Service.DTOs;
using EmployeesTasks.ViewModels.EmployeeTasks;

namespace EmployeesTasks.ViewModels.Mappers
{
	public static class EmployeeTaskMapper
	{
		public static EmployeeTaskViewModel ToViewModel(this EmployeeTaskDTO taskDTO)
		{
			return new EmployeeTaskViewModel()
			{
				Id = taskDTO.Id,
				EmployeeId = taskDTO.EmployeeId,
				Title = taskDTO.Title,
				Description = taskDTO.Description,
				DueDate = taskDTO.DueDate,
			};
		}

		public static EmployeeTaskDTO ToDTO(this EmployeeTaskViewModel taskViewModel)
		{
			return new EmployeeTaskDTO()
			{
				Id = taskViewModel.Id,
				EmployeeId = taskViewModel.EmployeeId,
				Title = taskViewModel.Title,
                Description = taskViewModel.Description,
				DueDate = taskViewModel.DueDate,
			};
		}
	}
}
