﻿using System.Collections.Generic;

namespace EmployeesTasks.ViewModels.Employees
{
	public class EmployeesViewModel
	{
		public IEnumerable<EmployeeViewModel> Employees { get; set; } = new List<EmployeeViewModel>();
	}
}
