﻿using EmployeesTasks.Data.Models;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeesTasks.ViewModels.Employees
{
	public class EmployeeViewModel
	{
		public int Id { get; set; }

		[Required(ErrorMessage = "The Name field is required.")]
		[StringLength(50, MinimumLength = 5, ErrorMessage = "The Name length must be between 5 and 50.")]
		public string FullName { get; set; }


		[Required(ErrorMessage = "The Email field is required.")]
		[RegularExpression(@"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}$", ErrorMessage = "The Email field is not a valid email address.")]
		public string Email { get; set; }


		[Required(ErrorMessage = "The Phone Number field is required.")]
		[RegularExpression(@"^\+359\s*[\d()\-\s]{6,15}\d$", ErrorMessage = "The field is not a valid phone number.")]
		public string PhoneNumber { get; set; }


		[Required(ErrorMessage = "The Date of Birth field is required.")]
		public DateTime DateOfBirth { get; set; }


		[Required(ErrorMessage = "The Monthly Salary field is required.")]
		public int MonthlySalary { get; set; }

		public int CompletedTasks { get; set; }

		public List<EmployeeTask> Tasks { get; set; }
	}
}
