﻿using EmployeesTasks.Data.Models;
using EmployeesTasks.Service.DTOs;


namespace EmployeesTasks.Service.Mappers
{
	public static class EmployeeTaskMapper
	{
		public static EmployeeTaskDTO ToDTO(this EmployeeTask task)
		{
			return new EmployeeTaskDTO()
			{
				Id = task.Id,
				EmployeeId = task.EmployeeId,
				Title = task.Title,
				Description = task.Description,
				DueDate = task.DueDate,
				DateOfCompletion = task.DateOfCompletion,
				IsDone = task.IsDone,
				IsDeleted = task.IsDeleted,
			};
		}

		public static EmployeeTask ToEntity(this EmployeeTaskDTO taskDTO)
		{
			return new EmployeeTask()
			{
				Id = taskDTO.Id,
				EmployeeId = taskDTO.EmployeeId,
                Title = taskDTO.Title,
				Description = taskDTO.Description,
				DueDate = taskDTO.DueDate,
				DateOfCompletion = taskDTO.DateOfCompletion,
				IsDone = taskDTO.IsDone,
				IsDeleted = taskDTO.IsDeleted,
			};
		}
	}
}
