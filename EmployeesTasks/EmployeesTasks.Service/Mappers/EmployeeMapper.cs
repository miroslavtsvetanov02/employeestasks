﻿using EmployeesTasks.Data.Models;
using EmployeesTasks.Service.DTOs;

namespace EmployeesTasks.Service.Mappers
{
	public static class EmployeeMapper
	{
		public static EmployeeDTO ToDTO(this Employee employee)
		{
			return new EmployeeDTO()
			{
				Id = employee.Id,
				FullName = employee.FullName,
				Email = employee.Email,
				PhoneNumber = employee.PhoneNumber,
				DateOfBirth = employee.DateOfBirth,
				MonthlySalary = employee.MonthlySalary,
				CompletedTasks = employee.CompletedTasks,
				Tasks = employee.Tasks,
				IsDeleted = employee.IsDeleted
			};
		}

		public static Employee ToEntity(this EmployeeDTO employeeDTO)
		{
			return new Employee()
			{
				Id = employeeDTO.Id,
				FullName = employeeDTO.FullName,
				Email = employeeDTO.Email,
				PhoneNumber = employeeDTO.PhoneNumber,
				DateOfBirth = employeeDTO.DateOfBirth,
				MonthlySalary = employeeDTO.MonthlySalary,
				CompletedTasks = employeeDTO.CompletedTasks,
				Tasks = employeeDTO.Tasks,
				IsDeleted = employeeDTO.IsDeleted
			};
		}
	}
}
