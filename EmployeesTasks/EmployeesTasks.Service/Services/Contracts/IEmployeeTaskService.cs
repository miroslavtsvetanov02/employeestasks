﻿using EmployeesTasks.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeesTasks.Service.Services.Contracts
{
    public interface IEmployeeTaskService
	{
        Task<EmployeeTaskDTO> GetByIdAsync(int id);
        Task<List<EmployeeTaskDTO>> GetAllAsync();
  

        Task<EmployeeTaskDTO> CreateTaskAsync(EmployeeTaskDTO taskDTO);
        Task<EmployeeTaskDTO> UpdateTaskAsync(EmployeeTaskDTO taskDTO);

        Task DeleteAsync(int id);
        Task AssignTask(int taskId, int employeeId);
        Task CompleteTask(int taskId);
	}
}
