﻿using EmployeesTasks.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeesTasks.Service.Services.Contracts
{
	public interface IEmployeeService
	{
		Task<EmployeeDTO> GetByIdAsync(int id);
		Task<List<EmployeeDTO>> GetAllAsync();
		Task<List<EmployeeDTO>> GetTopEmployeesAsync();


        Task<EmployeeDTO> CreateEmployeeAsync(EmployeeDTO employeeDTO);
		Task<EmployeeDTO> UpdateEmployeeAsync(EmployeeDTO employeeDTO);
		Task DeleteAsync(int id);
	}
}
