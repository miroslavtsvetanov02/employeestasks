﻿using EmployeesTasks.Data.Database;
using EmployeesTasks.Data.Models;
using EmployeesTasks.Service.Constants;
using EmployeesTasks.Service.DTOs;
using EmployeesTasks.Service.Exceptions;
using EmployeesTasks.Service.Mappers;
using EmployeesTasks.Service.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTasks.Service.Services
{
    public class EmployeeTaskService : IEmployeeTaskService
	{
		private readonly EmployeesTasksDbContext _database;

		public EmployeeTaskService(EmployeesTasksDbContext database)
		{
			_database = database;
		}

        public async Task<List<EmployeeTaskDTO>> GetAllAsync()
        {
            var tasks = await _database.Tasks
                .Where(x => !x.IsDeleted && !x.IsDone)
                .Select(x => x.ToDTO())
                .ToListAsync();

            return tasks;
        }

        public async Task<EmployeeTaskDTO> GetByIdAsync(int id)
        {
            var task = await _database.Tasks.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted)
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(EmployeeTask)));

			return task.ToDTO();
        }

        public async Task<EmployeeTaskDTO> CreateTaskAsync(EmployeeTaskDTO taskDTO)
        {

            EmployeeTask taskToAdd = taskDTO.ToEntity();

            await _database.Tasks.AddAsync(taskToAdd);
            await _database.SaveChangesAsync();

            return taskToAdd.ToDTO();
        }

        public async Task<EmployeeTaskDTO> UpdateTaskAsync(EmployeeTaskDTO taskDTO)
        {
            var taskToBeUpdated = await _database.Tasks
                .FirstOrDefaultAsync(x => x.Id == taskDTO.Id && !x.IsDeleted);

            taskToBeUpdated.Title = taskDTO.Title;
            taskToBeUpdated.Description = taskDTO.Description;
            taskToBeUpdated.DueDate = taskDTO.DueDate;

            await _database.SaveChangesAsync();

            return taskToBeUpdated.ToDTO();
        }

        public async Task DeleteAsync(int id)
        {
            var taskToBeDeleted = await _database.Tasks
                .FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);

            taskToBeDeleted.EmployeeId = null;
            taskToBeDeleted.IsDeleted = true;

            await _database.SaveChangesAsync();
        }

        public async Task AssignTask(int taskId, int employeeId)
        {
            var task = await _database.Tasks
                .FirstOrDefaultAsync(x => x.Id == taskId && !x.IsDeleted);

            var employee = await _database.Employees
                .FirstOrDefaultAsync(x => x.Id == employeeId && !x.IsDeleted);

            task.EmployeeId = employee.Id;

            await _database.SaveChangesAsync();
        }

        public async Task CompleteTask(int taskId)
        {
			var task = await _database.Tasks.FirstOrDefaultAsync(x => x.Id == taskId && !x.IsDeleted);

			var employee = await _database.Employees.FirstOrDefaultAsync(x => x.Id == task.EmployeeId);

            if (task.EmployeeId != null)
            {
                employee.CompletedTasks++;
                task.DateOfCompletion = DateTime.Now;
                task.IsDone = true;
            }
            else
            {
                task.IsDeleted = true;
            }

			await _database.SaveChangesAsync();
		}

    }
}
