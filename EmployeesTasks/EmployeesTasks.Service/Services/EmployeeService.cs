﻿using EmployeesTasks.Data.Database;
using EmployeesTasks.Data.Models;
using EmployeesTasks.Service.Constants;
using EmployeesTasks.Service.DTOs;
using EmployeesTasks.Service.Exceptions;
using EmployeesTasks.Service.Mappers;
using EmployeesTasks.Service.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTasks.Service.Services
{
	public class EmployeeService : IEmployeeService
	{

		private readonly EmployeesTasksDbContext _database;

		public EmployeeService(EmployeesTasksDbContext database)
		{
			_database = database;
		}


		public async Task<List<EmployeeDTO>> GetAllAsync()
		{
			var employees = await _database.Employees
				.Where(x => !x.IsDeleted)
				.Select(x => x.ToDTO())
				.ToListAsync();

			return employees;
		}


		public async Task<EmployeeDTO> GetByIdAsync(int id)
		{
			var employee = await _database.Employees.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted)
				?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Employee)));

            return employee.ToDTO();
		}


        public async Task<List<EmployeeDTO>> GetTopEmployeesAsync()
        {

            var cutoffDate = DateTime.Today.AddMonths(-1);
            var recentTasks = await _database.Tasks
                .Where(t => t.DateOfCompletion >= cutoffDate)
                .ToListAsync();

            // Group tasks by employee ID and count the number of completed tasks for each employee.
            var employeeTaskCounts = recentTasks
                .GroupBy(t => t.EmployeeId)
                .ToDictionary(g => g.Key, g => g.Count());

            // Get the top 5 employees with the most completed tasks.
            var topEmployeeIds = employeeTaskCounts
                .OrderByDescending(kv => kv.Value)
                .Take(5)
                .Select(kv => kv.Key)
                .ToList();

            // Get the employee details for the top 5 employees.
            var topEmployees = await _database.Employees
                .Where(e => topEmployeeIds.Contains(e.Id) && !e.IsDeleted)
                .Select(e => e.ToDTO())
                .ToListAsync();

            // Set the employee ID property of each EmployeeDTO object.
            foreach (var employee in topEmployees)
            {
                employee.Id = topEmployeeIds.IndexOf(employee.Id) + 1;
            }

            return topEmployees;
        }


        public async Task<EmployeeDTO> CreateEmployeeAsync(EmployeeDTO employeeDTO)
		{

			Employee employeeToAdd = employeeDTO.ToEntity();

			await _database.Employees.AddAsync(employeeToAdd);
			await _database.SaveChangesAsync();

			return employeeToAdd.ToDTO();
		}


		public async Task<EmployeeDTO> UpdateEmployeeAsync(EmployeeDTO employeeDTO)
		{
			var employeeToBeUpdated = await _database.Employees
				.FirstOrDefaultAsync(x => x.Id == employeeDTO.Id && !x.IsDeleted);

			employeeToBeUpdated.FullName = employeeDTO.FullName;
			employeeToBeUpdated.Email = employeeDTO.Email;
			employeeToBeUpdated.PhoneNumber = employeeDTO.PhoneNumber;
			employeeToBeUpdated.MonthlySalary = employeeDTO.MonthlySalary;
			employeeToBeUpdated.DateOfBirth = employeeDTO.DateOfBirth;

			await _database.SaveChangesAsync();

			return employeeToBeUpdated.ToDTO();
		}

		public async Task DeleteAsync(int id)
		{
			var employeeToBeDeleted = await _database.Employees
				.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);

			employeeToBeDeleted.IsDeleted = true;

			await _database.SaveChangesAsync();
		}
	}
}
