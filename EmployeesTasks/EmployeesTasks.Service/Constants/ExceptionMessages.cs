﻿namespace EmployeesTasks.Service.Constants
{
    public class ExceptionMessages
    {
        //General
        public const string NotFound = "Cannot find {0} with that id!";
        public const string YouCannotDoThat = "You cannot do this action!";
        public const string Duplicate = "{0} with that {1} already exsists!";
        public const string InvalidData = "The data that you have entered is invalid!";
    }
}
