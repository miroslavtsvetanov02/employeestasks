﻿using EmployeesTasks.Service.DTOs.Abstract;
using System;

namespace EmployeesTasks.Service.DTOs
{
	public class EmployeeTaskDTO : BaseDTO
	{
		public int? EmployeeId { get; set; }

		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime DueDate { get; set; }
        public DateTime? DateOfCompletion { get; set; }

        public bool IsDone { get; set; }
	}
}
