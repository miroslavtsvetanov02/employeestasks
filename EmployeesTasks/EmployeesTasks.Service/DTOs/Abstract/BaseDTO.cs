﻿namespace EmployeesTasks.Service.DTOs.Abstract
{
	public abstract class BaseDTO
	{
		public int Id { get; set; }
		public bool IsDeleted { get; set; }
	}
}
