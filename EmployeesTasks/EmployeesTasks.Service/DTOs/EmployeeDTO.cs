﻿using EmployeesTasks.Data.Models;
using EmployeesTasks.Service.DTOs.Abstract;
using System;
using System.Collections.Generic;

namespace EmployeesTasks.Service.DTOs
{
	public class EmployeeDTO : BaseDTO
	{

		public string FullName { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public DateTime DateOfBirth { get; set; }
		public int MonthlySalary { get; set; }
		public int CompletedTasks { get; set; }

		public List<EmployeeTask> Tasks { get; set; }
	}
}
