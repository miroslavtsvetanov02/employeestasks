﻿using EmployeesTasks.Data.Models;
using EmployeesTasks.Data.SeedData;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EmployeesTasks.Data.Database
{
	public class EmployeesTasksDbContext : IdentityDbContext
	{
		public EmployeesTasksDbContext(DbContextOptions<EmployeesTasksDbContext> options)
			: base(options)
		{
		}

		public virtual DbSet<Employee> Employees { get; set; }
		public virtual DbSet<EmployeeTask> Tasks { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();
        }

		public override int SaveChanges()
		{
			UpdateSoftDeleteStatuses();
			return base.SaveChanges();
		}

		public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
		{
			UpdateSoftDeleteStatuses();
			return base.SaveChangesAsync(cancellationToken);
		}

		private void UpdateSoftDeleteStatuses()
		{
			foreach (var entry in ChangeTracker.Entries())
			{
				if (entry.GetType().GetProperty("isDeleted") == null)
				{
					continue;
				}
				switch (entry.State)
				{
					case EntityState.Added:
						entry.CurrentValues["isDeleted"] = false;
						break;
					case EntityState.Deleted:
						entry.State = EntityState.Modified;
						entry.CurrentValues["isDeleted"] = true;
						break;
				}
			}
		}
	}
}
