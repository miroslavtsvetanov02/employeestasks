﻿using EmployeesTasks.Data.Abstract;
using System;

namespace EmployeesTasks.Data.Models
{
	public class EmployeeTask : BaseModel
	{
		//Foreign key (the employee who is assigned)
		public int? EmployeeId { get; set; }

		//Navigation property
		public Employee Employee { get; set; }

		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime? DateOfCompletion { get; set; }

		public bool IsDone { get; set; }

	}
}
