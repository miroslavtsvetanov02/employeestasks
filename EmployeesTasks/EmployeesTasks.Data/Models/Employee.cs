﻿using EmployeesTasks.Data.Abstract;
using System;
using System.Collections.Generic;

namespace EmployeesTasks.Data.Models
{
	public class Employee : BaseModel
	{
		public string FullName { get; set; }

		public string Email { get; set; }

		public string PhoneNumber { get; set; }

		public DateTime DateOfBirth { get; set; }

		public int MonthlySalary { get; set; }

		public int CompletedTasks { get; set; }


        //Navigation property (list of the tasks which one person have)
        public List<EmployeeTask> Tasks { get; set; }
	}
}
