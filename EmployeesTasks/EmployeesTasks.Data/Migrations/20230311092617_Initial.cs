﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesTasks.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MonthlySalary = table.Column<int>(type: "int", nullable: false),
                    CompletedTasks = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeId = table.Column<int>(type: "int", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DueDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateOfCompletion = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDone = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "CompletedTasks", "DateOfBirth", "Email", "FullName", "IsDeleted", "MonthlySalary", "PhoneNumber" },
                values: new object[,]
                {
                    { 1, 0, new DateTime(1990, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "gosho.ivanov@gmail.com", "Georgi Ivanov", false, 2500, "+359883421650" },
                    { 16, 5, new DateTime(1990, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "m.spasov@gmail.com", "Miroslav Spasov", false, 1440, "+359883482150" },
                    { 15, 3, new DateTime(1994, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "tanya.petkova@gmail.com", "Tanya Petkova", false, 2500, "+359872754620" },
                    { 14, 14, new DateTime(1998, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "d.ivanov@gmail.com", "Dobromir Ivanov", false, 1900, "+359885375650" },
                    { 13, 10, new DateTime(1990, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "stefka.spasova@gmail.com", "Stefka Spasova", false, 2500, "+359883323750" },
                    { 12, 13, new DateTime(1990, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "m.atanasova@gmail.com", "Mirela Atanasova", false, 1440, "359883236350" },
                    { 11, 20, new DateTime(1994, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "georgi.spasov@gmail.com", "Georgi Spasov", false, 2500, "+359874456950" },
                    { 10, 6, new DateTime(1998, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "y.atanasov@gmail.com", "Yodran Atanasov", false, 1900, "+359885421140" },
                    { 9, 4, new DateTime(1990, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "tosho.tsvetkov@gmail.com", "Tosho Tsvetkov", false, 2500, "+359883421670" },
                    { 8, 5, new DateTime(1990, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "marina.tomkova@gmail.com", "Marina Tomkova", false, 1440, "+359883486350" },
                    { 7, 3, new DateTime(1994, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "miroslav.mironov@gmail.com", "Miroslav Mironov", false, 2500, "+359872756950" },
                    { 6, 13, new DateTime(1998, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "ivan.ivanov@gmail.com", "Ivan Ivanov", false, 1900, "+359885421650" },
                    { 5, 14, new DateTime(1990, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "cveti.cvetanov@gmail.com", "Cvetelin Cvetanov", false, 2500, "+359883324650" },
                    { 4, 15, new DateTime(1990, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "marina.georgieva@gmail.com", "Marina Georgieva", false, 1440, "+359883486350" },
                    { 3, 20, new DateTime(1994, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "miroslav.tsvetanov@gmail.com", "Miroslav Tsvetanov", false, 2500, "+359872756950" },
                    { 2, 3, new DateTime(1998, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "ivan.ivanov@gmail.com", "Ivan Ivanov", false, 1900, "+359885421650" }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "DateOfCompletion", "Description", "DueDate", "EmployeeId", "IsDeleted", "IsDone", "Title" },
                values: new object[,]
                {
                    { 5, null, "The database needs to be fixed ASAP.", new DateTime(2023, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, false, "Update the database" },
                    { 10, new DateTime(2023, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The keyboard needs fixing.", new DateTime(2023, 3, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, false, "Fix keyboard" }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "DateOfCompletion", "Description", "DueDate", "EmployeeId", "IsDeleted", "IsDone", "Title" },
                values: new object[,]
                {
                    { 3, new DateTime(2023, 2, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "The api is not working as intended. Please fix it.", new DateTime(2023, 3, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, false, false, "Fix the api" },
                    { 7, null, "The js script is not working again!", new DateTime(2023, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, false, false, "Fix the JS script" },
                    { 2, new DateTime(2023, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The frontend viewmodel is not working as intended. Please fix it.", new DateTime(2023, 3, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, false, false, "Fix the frontend viewmodel" },
                    { 4, null, "The view is not working as intended. Please fix it.", new DateTime(2023, 3, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, false, false, "Fix the view" },
                    { 8, new DateTime(2023, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The view is not working as intended. Please fix it.", new DateTime(2023, 3, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, false, false, "Fix the view" },
                    { 1, new DateTime(2023, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The backend model is not working as intended. Please fix it.", new DateTime(2023, 3, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, false, false, "Fix the backend model" },
                    { 9, null, "The cloud needs to be fixed", new DateTime(2023, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, false, false, "Fix cloud" },
                    { 6, null, "The computer is not working so it needs a little smackin.", new DateTime(2023, 3, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, false, false, "Fix the computer." }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_EmployeeId",
                table: "Tasks",
                column: "EmployeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Employees");
        }
    }
}
