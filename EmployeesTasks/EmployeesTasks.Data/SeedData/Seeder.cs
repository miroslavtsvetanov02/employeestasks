﻿using EmployeesTasks.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace EmployeesTasks.Data.SeedData
{
    public static class Seeder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var Employees = new List<Employee>()
            {
                new Employee
                {
                    Id = 1,
                    FullName = "Georgi Ivanov",
                    Email = "gosho.ivanov@gmail.com",
                    PhoneNumber = "+359883421650",
                    DateOfBirth = new DateTime(1990, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 0,
                },
                new Employee
                {
                    Id = 2,
                    FullName = "Ivan Ivanov",
                    Email = "ivan.ivanov@gmail.com",
                    PhoneNumber = "+359885421650",
                    DateOfBirth = new DateTime(1998, 1, 3),
                    MonthlySalary = 1900,
                    CompletedTasks = 3,
                },
                new Employee
                {
                    Id = 3,
                    FullName = "Miroslav Tsvetanov",
                    Email = "miroslav.tsvetanov@gmail.com",
                    PhoneNumber = "+359872756950",
                    DateOfBirth = new DateTime(1994, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 20,
                },
                new Employee
                {
                    Id = 4,
                    FullName = "Marina Georgieva",
                    Email = "marina.georgieva@gmail.com",
                    PhoneNumber = "+359883486350",
                    DateOfBirth = new DateTime(1990, 3, 3),
                    MonthlySalary = 1440,
                    CompletedTasks = 15,
                },
                new Employee
                {
                    Id = 5,
                    FullName = "Cvetelin Cvetanov",
                    Email = "cveti.cvetanov@gmail.com",
                    PhoneNumber = "+359883324650",
                    DateOfBirth = new DateTime(1990, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 14,
                },
                new Employee
                {
                    Id = 6,
                    FullName = "Ivan Ivanov",
                    Email = "ivan.ivanov@gmail.com",
                    PhoneNumber = "+359885421650",
                    DateOfBirth = new DateTime(1998, 1, 3),
                    MonthlySalary = 1900,
                    CompletedTasks = 13,
                },
                new Employee
                {
                    Id = 7,
                    FullName = "Miroslav Mironov",
                    Email = "miroslav.mironov@gmail.com",
                    PhoneNumber = "+359872756950",
                    DateOfBirth = new DateTime(1994, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 3,
                },
                new Employee
                {
                    Id = 8,
                    FullName = "Marina Tomkova",
                    Email = "marina.tomkova@gmail.com",
                    PhoneNumber = "+359883486350",
                    DateOfBirth = new DateTime(1990, 3, 3),
                    MonthlySalary = 1440,
                    CompletedTasks = 5,
                },
                new Employee
                {
                    Id = 9,
                    FullName = "Tosho Tsvetkov",
                    Email = "tosho.tsvetkov@gmail.com",
                    PhoneNumber = "+359883421670",
                    DateOfBirth = new DateTime(1990, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 4,
                },
                new Employee
                {
                    Id = 10,
                    FullName = "Yodran Atanasov",
                    Email = "y.atanasov@gmail.com",
                    PhoneNumber = "+359885421140",
                    DateOfBirth = new DateTime(1998, 1, 3),
                    MonthlySalary = 1900,
                    CompletedTasks = 6,
                },
                new Employee
                {
                    Id = 11,
                    FullName = "Georgi Spasov",
                    Email = "georgi.spasov@gmail.com",
                    PhoneNumber = "+359874456950",
                    DateOfBirth = new DateTime(1994, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 20,
                },
                new Employee
                {
                    Id = 12,
                    FullName = "Mirela Atanasova",
                    Email = "m.atanasova@gmail.com",
                    PhoneNumber = "359883236350",
                    DateOfBirth = new DateTime(1990, 3, 3),
                    MonthlySalary = 1440,
                    CompletedTasks = 13,
                },
                new Employee
                {
                    Id = 13,
                    FullName = "Stefka Spasova",
                    Email = "stefka.spasova@gmail.com",
                    PhoneNumber = "+359883323750",
                    DateOfBirth = new DateTime(1990, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 10,
                },
                new Employee
                {
                    Id = 14,
                    FullName = "Dobromir Ivanov",
                    Email = "d.ivanov@gmail.com",
                    PhoneNumber = "+359885375650",
                    DateOfBirth = new DateTime(1998, 1, 3),
                    MonthlySalary = 1900,
                    CompletedTasks = 14,
                },
                new Employee
                {
                    Id = 15,
                    FullName = "Tanya Petkova",
                    Email = "tanya.petkova@gmail.com",
                    PhoneNumber = "+359872754620",
                    DateOfBirth = new DateTime(1994, 1, 3),
                    MonthlySalary = 2500,
                    CompletedTasks = 3,
                },
                new Employee
                {
                    Id = 16,
                    FullName = "Miroslav Spasov",
                    Email = "m.spasov@gmail.com",
                    PhoneNumber = "+359883482150",
                    DateOfBirth = new DateTime(1990, 3, 3),
                    MonthlySalary = 1440,
                    CompletedTasks = 5,
                },

            };
            modelBuilder.Entity<Employee>().HasData(Employees);

            var Tasks = new List<EmployeeTask>()
            {
                new EmployeeTask
                {
                    Id = 1,
                    EmployeeId = 3,
                    Title = "Fix the backend model",
                    Description = "The backend model is not working as intended. Please fix it.",
                    DueDate = new DateTime(2023, 3, 13),
                    DateOfCompletion = new DateTime(2023, 3, 1)
                },
                new EmployeeTask
                {
                    Id = 2,
                    EmployeeId = 2,
                    Title = "Fix the frontend viewmodel",
                    Description = "The frontend viewmodel is not working as intended. Please fix it.",
                    DueDate = new DateTime(2023, 3, 16),
                    DateOfCompletion = new DateTime(2023, 3, 1)
                },
                new EmployeeTask
                {
                    Id = 3,
                    EmployeeId = 1,
                    Title = "Fix the api",
                    Description = "The api is not working as intended. Please fix it.",
                    DueDate = new DateTime(2023, 3, 14),
                    DateOfCompletion = new DateTime(2023, 2, 5)
                },
                new EmployeeTask
                {
                    Id = 4,
                    EmployeeId = 2,
                    Title = "Fix the view",
                    Description = "The view is not working as intended. Please fix it.",
                    DueDate = new DateTime(2023, 3, 16),
                    DateOfCompletion = null
                },
                new EmployeeTask
                {
                    Id = 5,
                    EmployeeId = null,
                    Title = "Update the database",
                    Description = "The database needs to be fixed ASAP.",
                    DueDate = new DateTime(2023, 3, 20),
                    DateOfCompletion = null
                },
                new EmployeeTask
                {
                    Id = 6,
                    EmployeeId = 6,
                    Title = "Fix the computer.",
                    Description = "The computer is not working so it needs a little smackin.",
                    DueDate = new DateTime(2023, 3, 17),
                    DateOfCompletion = null
                },
                new EmployeeTask
                {
                    Id = 7,
                    EmployeeId = 1,
                    Title = "Fix the JS script",
                    Description = "The js script is not working again!",
                    DueDate = new DateTime(2023, 3, 15),
                    DateOfCompletion = null
                },
                new EmployeeTask
                {
                    Id = 8,
                    EmployeeId = 2,
                    Title = "Fix the view",
                    Description = "The view is not working as intended. Please fix it.",
                    DueDate = new DateTime(2023, 3, 11),
                    DateOfCompletion = new DateTime(2023, 2, 1)
                },
                new EmployeeTask
                {
                    Id = 9,
                    EmployeeId = 4,
                    Title = "Fix cloud",
                    Description = "The cloud needs to be fixed",
                    DueDate = new DateTime(2023, 3, 15),
                    DateOfCompletion = null
                },
                new EmployeeTask
                {
                    Id = 10,
                    EmployeeId = null,
                    Title = "Fix keyboard",
                    Description = "The keyboard needs fixing.",
                    DueDate = new DateTime(2023, 3, 11),
                    DateOfCompletion = new DateTime(2023, 2, 1)
                },

            };
            modelBuilder.Entity<EmployeeTask>().HasData(Tasks);
        }
    }
}
